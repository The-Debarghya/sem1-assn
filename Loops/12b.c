#include<stdio.h>
int main(){
	int n, x;
	printf("enter no. of rows ");
	scanf("%d", &n);
	x = n;
	for(int i=1; i <= n; i++){
		for(int j=x; j >= 1; j--){
			printf("  ");
		}
		for(int k=1; k <= i; k++){
			printf("%d ", k);
		}
		printf("\n");
		x = x-1;
	}
	return 0;
}
