#include<stdio.h>
#include<math.h>
#include<stdlib.h>
double ln(double x){
	double num, y, cal, sum = 0; 
    num = (x - 1) / (x + 1);  
    for (int i = 1; i <= 1000; i++) { 
        y = (2 * i) - 1; 
        cal = (pow(num, y))/y;  
        sum = sum + cal; 
    } 
    sum = 2 * sum; 
    return sum;
}
int main(){
	
	float x;
	int n;
	
	printf("Enter base of logarithm (2 to 10), (0 for natural logarithm) ");
	scanf("%d", &n);
	printf("Enter the value of which logarithm is required ");
	scanf("%f", &x);
	
	switch(n){
		case 0:
			printf("%f", ln(x));
			break;
		case 2:
			printf("%f", (ln(x)/ln(2)));
			break;
		case 3:
			printf("%f", (ln(x)/ln(3)));
			break;
		case 4:
			printf("%f", (ln(x)/ln(4)));
			break;
		case 5:
			printf("%f", (ln(x)/ln(5)));
			break;
		case 6:
			printf("%f", (ln(x)/ln(6)));
			break;
		case 7:
			printf("%f", (ln(x)/ln(7)));
			break;
		case 8:
			printf("%f", (ln(x)/ln(8)));
			break;
		case 9:
			printf("%f", (ln(x)/ln(9)));
			break;
		case 10:
			printf("%f", (ln(x)/ln(10)));
			break;
		default:
			printf("Wrong input");
			break;
	}
	printf("\n");
	char r;
	printf("Do you want to continue? press y or n \n");
	r = getch();
	if(r == 'y'){
		main();
	}
	else{
		exit(0);
	}
	return 0;
}
