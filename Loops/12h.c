#include<stdio.h>
int main(){
	int n;
	printf("Enter the limit ");
	scanf("%d", &n);
	for(int i=1; i <= n; i++){
		for(int j=1; j <= n-i; j++){
			printf(" ");
		}
		for(int k=1; k <= i; k++){
			if(k == 1){
				printf("+");
			}
			else{
				printf(" ");
			}
		}
		for(int l=i-1; l >= 1; l--){
			if(l == 1){
				printf("+");
			}
			else{
				printf(" ");
			}
		}
		printf("\n");
	}
	for(int m=n-1; m >= 1; m--){
		for(int o=1; o <= n-m; o++){
			printf(" ");
		}
		for(int p=1; p <= m; p++){
			if(p == 1){
				printf("+");
			}
			else{
				printf(" ");
			}
		}
		for(int q=m-1; q >= 1; q--){
			if(q == 1){
				printf("+");
			}
			else{
				printf(" ");
			}
		}
		printf("\n");
	}
	return 0;
}
