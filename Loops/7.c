#include<stdio.h>
int main(){
	long long int first=0, second=1, third;
	int n;
	printf("Enter no. of terms you want ");
	scanf("%d", &n);
	printf("the fibonacci sequence upto %d terms : \n", n);
	while (n != 0){
		printf("%lld ", first);
		third = first + second;
		first = second;
		second = third;
		n = n - 1;
	}
	return 0;
}
