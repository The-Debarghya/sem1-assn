#include<stdio.h>
int fact(int x){
	if (x == 0){
		return 1;
	}
	else{
		return (x*fact(x-1));
	}
}
int main(){
	int n;
	long long int sum=0;
	printf("Enter no. upto which you want sum ");
	scanf("%d", &n);
	for(int i=1; i <= n; i++){
		sum = sum + fact(i);
	}
	printf("%lld", sum);
	return 0;
}
