#include<stdio.h>
#include<math.h>
#include<string.h>
int main(){
	int  x;
	char n;
	printf("Enter the  no. you want to put o for octal, d for decimal, h for hexadecimal ");
	n = getchar();
	switch(n){
		case 'o' :
			printf("Enter the no. in octal ");
			scanf("%o", &x);
			printf("the no. in decimal is %d \n", x);
			printf("the no. in hexadecimal is %x \n", x);
			break;
		case 'd' :
			printf("Enter the no. in decimal ");
			scanf("%d", &x);
			printf("the no. in octal is %o \n", x);
			printf("the no. in hexadecimal is %x \n", x);
			break;
		case 'h' :
			printf("Enter the no. in hexadecimal ");
			scanf("%x", &x);
			printf("the no. in decimal is %d \n", x);
			printf("the no. in octal is %o \n", x);
			break;
		default :
			printf("Wrong base entered");
	}

	return 0;
}
