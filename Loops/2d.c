#include<stdio.h>
int factor(int x){
	int sum = 0;
	for(int i=1; i<=x; i++){
		if (x%i == 0){
			sum = sum + i;
		}
		else{
			continue;
		}
	}
	return sum;
}
int main(){
	int n;
	long long int sum = 0;
	printf("Enter no. upto which you want sum ");
	scanf("%d", &n);
	for(int i=1; i<=n; i++){
		sum = sum + factor(i);
	}
	printf("%lld", sum);
	return 0;
}
