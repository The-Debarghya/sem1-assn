#include<stdio.h>
int main(){
	int n;
	printf("Enter the limit ");
	scanf("%d", &n);
	for(int i=1; i <= 2*n-1; i++){
		for(int j=1; j <= 2*n-i; j++){
			printf(" ");
		}
		for(int k=1; k <= (i+1)/2; k++){
			printf("%2d", k);
		}
		for(int l=i/2; l > 0; l--){
			printf("%2d", l);
		}
		printf("\n");
	}
	return 0;
}
