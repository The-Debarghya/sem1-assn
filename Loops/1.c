#include<math.h>
#include<stdio.h>
void divisible(int n){
	printf("numbers divisible by %d between 1 and %d*5 are: \n", n, n);
	for(int i=1; i <= n*5; i++){
		if (i%n == 0){
			printf("%d ", i);
		}
		else{
			continue;
		}
	}
}
void primeno(int n){
    int count=0;
	printf("Prime numbers from 1 to %d are: \n", n);
	for(int i=1; i <= n; i++){
		int count = 0;
		for(int j=1; j<=i; j++){
			if (i%j == 0){
				count = count + 1;
			}
			else{
				continue;
			}
		}
		if (count == 2){
			printf("%d \n", i);
		}
	}
	
}
void primefac(int n){
	int count=0;
	printf("Prime factors of %d are: \n", n);
	for(int i=1; i <= abs(n); i++){
		count = 0;
		for(int j=1; j<=i; j++){
			if (i%j == 0){
				count = count + 1;
			}
			else{
				continue;
			}
		}
		if (count == 2 && n%i == 0){
			printf("%d ", i);
			
		}
	}
	
}
void dtoo(int x){
	if (x==0){
		return ;
	}
	else{
		dtoo(x/8);
		printf("%d", x%8);
	}
}
int sumofdigit(int n){
	int i, rem, sum = 0;
	i = n;
	while (n != 0){
		rem = n%10;
		sum = sum + rem;
		n = n/10;
	}
	return sum;
}
long long int fact(int x){
	if (x==0){
		return 1;
	}
	else{
		return(x*fact(x-1));
	}
}
int reverse(int n){
	int i, rev=0, rem;
	i = n;
	while (n != 0){
		rem = n%10;
		rev = rem+(rev*10);
		n = n/10;
	}
    return rev;
}
int main(){
	int n;
	printf("Enter the value of n ");
	scanf("%d", &n);
	divisible(n); printf("\n");
	primeno(n); printf("\n");
	primefac(n); printf("\n");
	printf("Octal equivalent of %d is ", n);
	dtoo(n);printf("\n");
	printf("sum of digits is %d \n", sumofdigit(n));
	printf("factorial of the number is %lld \n", fact(n));
	printf("reverse of the digits of %d is %d", n, reverse(n));
	return 0;
}
