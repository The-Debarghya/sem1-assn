#include<stdio.h>
#include<math.h>
#include<string.h>
#define pi 3.141592654
int n;
long double fact(int y){
	if(y == 0){
		return 1;
	}
	else{
		return (y*fact(y-1));
	}
		
}
float sine(float x){
	int  ct=1, j=1;
	float sum=0, y=0;
	printf("Enter no. of terms of taylor series you want to calculate ");
	scanf("%d", &n);
	while (ct <= n){
		if(ct%2!=0){
			y = pow(x, j);
			sum=sum+(y/fact(j));
		}
		else{
			y = pow(x, j);
			sum=sum-(y/fact(j));
		}
		j = j + 2;
		ct++;
		y = 0;
	}
	return sum; 
	
}
int main(){
	float x;
	char u[2];
	printf("Enter unit(r/R for radian, d/D for degree, g/G for gradian)");
	gets(u);
	printf("Enter value of angle ");
	scanf("%f", &x);
	x = x - ((float)(int)(x/(2*pi))*(2*pi));
	if (u[0] == 'r' || u[0] == 'R'){
		printf("sin(%f) = %f", x, sine(x));
	}
	else if(u[0] == 'd' || u[0] == 'D'){
		x = x*(pi/180);
		printf("sin(%f) = %f", x, sine(x));
	}
	else if(u[0] == 'g' || u[0] == 'G'){
		x = x*(pi/200);
		printf("sin(%f) = %f", x, sine(x));
		
	}
	else{
		printf("wrong letter input");
	}
    return 0;
}

