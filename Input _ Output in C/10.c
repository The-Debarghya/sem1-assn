#include<stdio.h>
#include<math.h>
int main(){
	float cen[2], cor[2], d;
	float r;
	printf("Enter the coordinates of the center of circle ");
	scanf("%f %f", &cen[0], &cen[1]);
	printf("Enter the radius of circle ");
	scanf("%f", &r);
	printf("Enter the coordinates of the point ");
	scanf("%f %f", &cor[0], &cor[1]);
	d = sqrt(pow((cen[0]-cor[0]), 2)+pow((cen[1]-cor[1]), 2));
	if (d>r){
		printf("the point is outside the circle");
	}
	if (d==r){
		printf("the point lies on the circle");
	}
	if(d<r){
		printf("the point is inside the circle");
	}
	return 0;
}
