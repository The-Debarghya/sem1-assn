#include<stdio.h>
#include<stdlib.h>
#include<string.h>
FILE *fp;
int varcount(char *str){
    int ct=0;
    for(int i=0; str[i] != '\0'; i++){
        if (str[i] == ','){
            ct = ct+1;
        }
    }
    return (ct+1);
}
int isfunc(char *str){
    for(int i=0; str[i] != '\0'; i++){
        if(str[i] == '(' || str[i] == ')' || str[i] == '{'){
            return 1;
        }
    }
    return 0;
}
int isptrarr(char *str){
    for(int i=0; str[i] != '\0'; i++){
        if(str[i] == '*' || str[i] == '['){
            return 1;
        }
    }
    return 0;
}
int main(){
    char lines[100][100], *temp, *token, *subtoken, c, *therest, *therest1, *words;
    int i=0, ct=0, ct1=0, ct2=0;
    fp = fopen("deb.c", "r");
    if(fp == NULL){
        printf("Error in opening the file\n");
        exit(0);
    }
    c = getc(fp);
    while(c != EOF){
        ungetc(c, fp);
        fgets(lines[i], 80, fp);
        i++;
        c = getc(fp);
    }
    /*printf("Test.c is given as:\n");
    for(int j=0; j<=i; j++){
        printf("%s", lines[j]);
    }*/
    for(int j=0; j<i; j++){
        temp = strdup(lines[j]);
        token = strtok_r(temp, "\n", &therest);
        for (words = token; ; words = NULL) {
            subtoken = strtok_r(words, " ,", &therest1);
            if (subtoken == NULL)
                break;
            if(isptrarr(subtoken)){
                    ct2 = ct2+1;
            }
            if (strcmp(subtoken, "int") == 0 || strcmp(subtoken, "char") == 0 || strcmp(subtoken, "float") == 0 || strcmp(subtoken, "double")==0){
                ct = ct + varcount(lines[j]);
                
                if(isfunc(lines[j])){
                    ct1 = ct1+1;
                }
            }                
        }
    } 
    printf("Total no. of variables in the file deb.c = %d\n", ct-ct1-ct2);
    return 0;
}
