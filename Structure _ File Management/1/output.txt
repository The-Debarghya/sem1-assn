

Q1 SAMPLE OUTPUT

No. of students' data you want to input:3
Enter name of student1:deb
Enter roll of student1:12
Enter student1 department:engg
Enter student1 course of study:cse
Enter the year of passing of student1:2019
Enter marks of 6 subjects of student1:30 50 60 50 70
80
Enter name of student2:dip
Enter roll of student2:13
Enter student2 department:engg
Enter student2 course of study:Mech
Enter the year of passing of student2:2018
Enter marks of 6 subjects of student2:80 80 60 20 82 89
Enter name of student3:arghya
Enter roll of student3:18
Enter student3 department:engg
Enter student3 course of study:etce
Enter the year of passing of student3:2019
Enter marks of 6 subjects of student3:40 60 30 55 77 30
*******************************************************************
Enter roll for reference:13
Name of the student is: dip
Marks he obtained in 6 subjects: 80 80 60 20 82 89
Department: engg  |  Course of study: Mech
Year of passing: 2018
Enter the year of passing for reference:2018
*******************************************************************
The students who passed in the year 2018 are:
dip 
*******************************************************************
THE STUDENTS ARRANGED IN DESCENDING ORDER OF THEIR MARKS AVERAGE:
Name      | Roll no. |     Marks in all subjects     | Average
 
dip		13		80 80 60 20 82 89	 68.000000 
deb		12		30 50 60 50 70 80	 56.000000 
arghya		18		40 60 30 55 77 30	 48.000000 
Do you want to store this in a text file?(y/n)
y
