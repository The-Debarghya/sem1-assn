#include<stdio.h>
#include<stdlib.h>
FILE *fp;
typedef struct{
	int subj1;
	int subj2;
	int subj3; 
	int subj4;
	int subj5;
	int subj6;
} subj;
struct students {
       char name[100];
       int roll;
       char dept[20];
	   char course[50];
	   int yop;
	   subj marks; 
	   float avg;
} ; 
void yearref(int yr, struct students st[100], int n){
	int y;
	for(int i=0; i<n; i++){
		y = st[i].yop;
		if(yr == y){
			printf("%s \n", st[i].name);
		}
	}
}
void rollref(int r, struct students st[100], int n){
	int rolln, i, c=1;
	for(i=0; i<n; i++){
		rolln = st[i].roll;
		if(rolln == r){
			break;
		}
		c = c+1;
	}
	if(c>n){
		printf("Roll not found\n");
		return;
	}
	printf("Name of the student is: %s\n", st[i].name);
	printf("Marks he obtained in 6 subjects: %d %d %d %d %d %d\n", st[i].marks.subj1, st[i].marks.subj2, st[i].marks.subj3, st[i].marks.subj4, st[i].marks.subj5, st[i].marks.subj6);
	printf("Department: %s  |  Course of study: %s\n", st[i].dept, st[i].course);
	printf("Year of passing: %d\n", st[i].yop);
}
void filewrite(struct students st[100], int n){
	fp = fopen("Studinfo.txt", "w+");
	fprintf(fp, "\nName      | Roll no. |  Department |  Course | Year of passing | Marks in all subjects\n \n");
	for(int i=0; i<n; i++){
		fprintf(fp, "%s \t \t%d \t%s \t\t %s \t %d \t   %d %d %d %d %d %d", st[i].name, st[i].roll, st[i].dept, st[i].course, st[i].yop, st[i].marks.subj1, st[i].marks.subj2, st[i].marks.subj3, st[i].marks.subj4, st[i].marks.subj5, st[i].marks.subj6);
		fprintf(fp, "\n");
	}
	fclose(fp);
}
float mean(struct students st[100], int j ){
	int total;
	float a;
	total = st[j].marks.subj1+st[j].marks.subj2+st[j].marks.subj3+st[j].marks.subj4+st[j].marks.subj5+st[j].marks.subj6;
	a = total/6;
	return a;
}
void sortst(struct students st[100], int n){
	int i, j;
	struct students temp;
	for(i=0; i<n; i++){
		for(j=i+1; j<n; j++){
			if(st[i].avg<st[j].avg){
				temp = st[i];
				st[i]= st[j];
				st[j] = temp;
			}
			else
				continue;
		}
	}
	printf("\nName      | Roll no. |     Marks in all subjects     | Average\n \n");
	for(int k=0; k<n; k++){
		printf("%s\t\t%d\t\t%d %d %d %d %d %d\t %f \n", st[k].name, st[k].roll, st[k].marks.subj1, st[k].marks.subj2, st[k].marks.subj3, st[k].marks.subj4, st[k].marks.subj5, st[k].marks.subj6, st[k].avg);
	}
}
int main(){
	int r, n;
	struct students st[100];
	printf("No. of students' data you want to input:");
	scanf("%d", &n);
	for(int i=0; i<n; i++){
		printf("Enter name of student%d:", i+1);
		scanf(" %[^\n]s", st[i].name);
		printf("Enter roll of student%d:", i+1);
	    scanf(" %d", &st[i].roll);
		printf("Enter student%d department:", i+1);
		scanf(" %[^\n]s", st[i].dept);
		printf("Enter student%d course of study:", i+1);
		scanf(" %[^\n]s", st[i].course);
		printf("Enter the year of passing of student%d:", i+1);
		scanf("%d", &st[i].yop);
		printf("Enter marks of 6 subjects of student%d:", i+1);
	    scanf(" %d %d %d %d %d %d", &st[i].marks.subj1, &st[i].marks.subj2, &st[i].marks.subj3, &st[i].marks.subj4, &st[i].marks.subj5, &st[i].marks.subj6);
	}printf("*******************************************************************\n");
	printf("Enter roll for reference:");
	scanf("%d", &r);
	rollref(r, st, n); int a;
	printf("Enter the year of passing for reference:");
	scanf("%d" , &a);
	printf("*******************************************************************\n");
	printf("The students who passed in the year %d are:\n", a);
	yearref(a, st, n);
	for(int j=0; j<n; j++){
		st[j].avg = mean(st, j);
	}
	printf("*******************************************************************\n");
	printf("THE STUDENTS ARRANGED IN DESCENDING ORDER OF THEIR MARKS AVERAGE:");
	sortst(st, n);
	char c;
	printf("Do you want to store this in a text file?(y/n)\n");
	scanf(" %c", &c);
	if(c == 'y'){
		filewrite(st, n);
		system("gedit Studinfo.txt \n");
	}
	else{
		exit(0);
	}
	return 0;
}
