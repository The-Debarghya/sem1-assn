#include<stdio.h>
#include<stdlib.h>
#include<string.h>
FILE *fp1, *fp2;
int main(){
    char words[100][80], temp[80];
    char c;
    int i = 0;
    fp1 = fopen("sort.txt", "r");
    if(fp1 == NULL){
        printf("Error in opening file \n");
    }
    c = getc(fp1);
    while(c != EOF){
        ungetc(c, fp1);
        fgets(words[i], 80, fp1);
        i++;
        c = getc(fp1);
    }
    printf("The actual list is:\n");
    for(int j=0; j<i; j++){
        printf("%s", words[j]);
    }
    printf("\n************************************************\n");
    for(int j=0;j<=i;j++){
        for(int k=j+1;k<=i;k++){
            if(strcmp(words[j],words[k])>0){
                strcpy(temp,words[j]);
                strcpy(words[j],words[k]);
                strcpy(words[k],temp);
            }
        }
    }
    printf("And here goes the sorted list:\n");
    for(int j=0; j<i; j++){
        printf("%s", words[j]);
    }
    fclose(fp1);
    return 0;
}