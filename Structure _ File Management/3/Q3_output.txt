

3c SAMPLE OUTPUT

How big you want the stack(No. of elements):4
1. Push
2. Pop
3. Exit
Enter your choice: 1
Enter data to push into stack: 10
Data pushed to stack.
1. Push
2. Pop
3. Exit
Enter your choice: 1
Enter data to push into stack: 20
Data pushed to stack.
1. Push
2. Pop
3. Exit
Enter your choice: 1
Enter data to push into stack: 30
Data pushed to stack.
1. Push
2. Pop
3. Exit
Enter your choice: 1
Enter data to push into stack: 60
Data pushed to stack.
1. Push
2. Pop
3. Exit
Enter your choice: 1
Enter data to push into stack: 78
Data pushed to stack.
1. Push
2. Pop
3. Exit
Enter your choice: 1
Enter data to push into stack: 87
Stack Overflow, can't add more element element to stack.
1. Push
2. Pop
3. Exit
Enter your choice: 2
Popped Data => 78
1. Push
2. Pop
3. Exit
Enter your choice: 3
**************************************************************

3f SAMPLE OUTPUT

The actual list is:
flowery
crack
wash
race
arrive
haunt
eight
risk
mysterious
normal
holiday
moan
toe
camp
helpless
slave
automatic
jobless
delight
tongue
aftermath
equal
sheep
direful
plant
wall
tacky
royal
flippant
bewildered
familiar
waggish
structure
material
colour
rabbits

************************************************
And here goes the sorted list:
aftermath
arrive
automatic
bewildered
camp
colour
crack
delight
direful
eight
equal
familiar
flippant
flowery
haunt
helpless
holiday
jobless
material
moan
mysterious
normal
plant
rabbits
race
risk
royal
sheep
slave
structure
tacky
toe
tongue
waggish
wall
