#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#define SIZE 100
int top = -1;
int stack[SIZE];
void push(int element, int size){
    if (top >= size){
        printf("Stack Overflow, can't add more element element to stack.\n");
        return;
    }
    top++;
    stack[top] = element;
    printf("Data pushed to stack.\n");
}
int pop(){
    if (top < 0){
        printf("Stack is empty.\n");
        return INT_MIN;
    }
    return stack[top--];
}
int main(){
    int choice, data, size;
    printf("How big you want the stack(No. of elements):");
    scanf("%d", &size);
    while(1){
        printf("1. Push\n");
        printf("2. Pop\n");
        printf("3. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);
        switch(choice){
            case 1: 
                printf("Enter data to push into stack: ");
                scanf("%d", &data);
                push(data, size);
                break;
            case 2: 
                data = pop();
                if (data != INT_MIN)
                    printf("Popped Data => %d\n", data);
                break;
            case 3: 
                exit(0);
                break;
            default: 
                printf("Invalid choice, please try again.\n");
        }
    }
    return 0;
}
