#include<stdio.h>
#include<string.h>
#include<stdlib.h>
struct directory{
    char name[30];
    long long int number;
};
struct directory d[20];
int i=0;
void add(){
    printf("Enter name : ");
    scanf(" %[^\n]s", d[i].name);
    printf("Enter phone number : ");
    scanf("%lld",&d[i].number);
    FILE *fp;
    fp=fopen("directory.txt","a");
    fprintf(fp,"%d.  Name : %s   Phone Number : %lld\n",i+1,d[i].name,d[i].number);
    fclose(fp);
    printf("Telephone directory updated successfully!!\n");
    i++;
}
void search(){
    char name[30];
    int flag=0;
    printf("Enter name to be searched: ");
    scanf(" %[^\n]s", name);
    int j=0;
    while(j<i){ 
        if(strcmp(name,d[j].name)==0){
            printf("\nContact found , phone number : %lld",d[j].number);
            flag=1;
        }
        j++;
    }
    if(flag==0)
        printf("No contact found with such name!!");
}
void delete(){
    FILE *fp;
    char name [30];
    printf("Enter name :");
    scanf(" %[^\n]s", name);
    int j=0, flag = 0;
    for(; j<=i; j++){
        if(strcmp(d[j].name, name) == 0){
            flag = 1;
            for(int l=0; l<strlen(name); l++){
                d[j].name[l] = '\0';
            }
            d[j].number = 0;
            break;
        }
    }
    if(flag == 0){
        printf("contact not found\n");
        return;
    }
    else{
        printf("contact deleted successfully");
    }
    fp = fopen("directory.txt", "w+");
    for(int k=0; k<i; k++){
        fprintf(fp,"%d.  Name : %s   Phone Number : %lld\n",k+1,d[k].name,d[k].number);
    }
    fclose(fp);
}
void browse(){
    FILE *fp;
    char ch;
    fp = fopen("directory.txt", "r");
    printf("\t\t\t\t-----------------PHONE DIRECTORY-------------------\n\n");
    ch = fgetc(fp);
    while (ch != EOF){        
        printf("%c", ch);
        ch = fgetc(fp);      
    }
    fclose (fp);    
}
void main(){
    int ch;
    do{   
        printf("\n1.Add contact\n2.Delete\n3.Search\n4.Browse\n5.Exit");
        printf("\nEnter choice : ");
        scanf("%d",&ch);
        switch(ch){
            case 1 : add();
                     break;
            case 2 : delete();
                     break;
            case 3 : search();
                     break;
            case 4 : browse();
                     break;
            case 5 : break;            
            default : printf("Wrong choice entered!!!!!!");
        }
    }while(ch!=5);
}