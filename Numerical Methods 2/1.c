#include<stdio.h>
#include<stdlib.h> 
#include<math.h>
void gauss_jordan( float a[20][20], int var ){
    float l;
    for ( int k = 0;k < var;k++ ){
        for ( int i = 0;i <= var;i++ ){
            l = a[ i ][ k ];
            for ( int j = 0;j <= var;j++ ){
                if ( i != k ){
                a[i][j] = (a[k][k]*a[i][j])-(l*a[k][j]);
				}
            }
        }
    }
    printf( "Solutions by Gauss Jordan method:\n" );
    for ( int i = 0;i < var; i++ ){
        printf( "The value of x%d is = %.2f\n", i + 1, ( float ) a[ i ][ var ] / ( float ) a[ i ][ i ] );
    }
}
void gauss_elim(float a[20][20], int var){
	int i, j, k;
	float p, x[20], s;
	for(k=0; k<=var-1; k++){
	for(i=k+1; i<var; i++){
	    p = a[i][k]/a[k][k];
	    for(j=k; j<=var; j++){
		a[i][j] = a[i][j] - (p * a[k][j]);
	    }
	}
    }
    x[var-1] = a[var-1][var] / a[var-1][var-1];
    for(i=var-2; i>=0; i--){
        s=0;
        for(j=i+1; j<var; j++){
            s += (a[i][j]*x[j]);
            x[i] = (a[i][var]-s)/a[i][i];
        }
    }
    printf("Solutions by naive Gauss elimination method:\n");
    for(i=0; i<var ; i++){
        printf("The value of x%d is = %.2f\n",i+1,x[i]);
    }
}
void jacobi(float a[20][20], int var, float e, int n){
	float big, x[20], x1[20], sum, temp, relerr;
	for(int i=0; i<var; i++){
		x[i] = 0;
	}
	for(int j=1; j<=n; j++){
		big = 0;
		for(int i=0; i<var; i++){
			sum = 0;
			for(int k=0; k<var; k++){
				if(k != i){
					sum = sum + a[i][k]*x[k];
				}
			}
			temp = (a[i][var] - sum)/a[i][i];
			relerr = fabs((x[i] - temp)/temp);
			if(relerr > big){
				big = relerr;
			}
          	x1[i] = temp;
		}
		for(int i=0; i<var; i++){
			x[i] = x1[i];
		}
		if (big <= e){
			printf("Solutions:\n");
			for(int l=0; l<var; l++){
				printf("x%d = %f \n", l+1, x[l]);
			}
			return ;
		}
	}
	printf("Solutions doesn't converge in %d no. of iterations", n);
}
int main(){
    int var, i, j , x, n;
	float a[20][20], e;
	
    printf( "Enter the number of variables:" );
    scanf( "%d", &var );
    for ( i = 0;i < var;i++ ){
        printf( "Enter the equation%d:", i + 1 );
        for ( j = 0;j < var;j++ ){
            printf( "Enter the coefficient of  x%d:", j + 1 );
            scanf( " %f", &a[ i ][ j ] );
        }
		printf( "Enter the constant:" );
        scanf( "%f", &a[ i ][ var] );
    }
	st : printf("In which method you want to calculate? \n(1. Naive Gauss Elimination, 2. Gauss Jordan elimination 3. Jacobi's method)");
	scanf(" %d", &x);
	if(x == 1){
		gauss_elim(a, var);
	}
	else if(x == 2){
		gauss_jordan( a, var );
	}
	else if(x == 3){
		printf("Permissible error:");
		scanf(" %f", &e);
		jacobi(a, var, e, 1000);
	}
	else{
		printf("Wrong input\n");
		goto st;
	}
    char c;
    printf("Do you want to continue?(y/n)");
    scanf(" %c", &c);
    if(c == 'y'){
        main();
    }
    else{
        exit(0);
    }
    return 0;
}
 
 
 
