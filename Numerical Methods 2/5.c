#include<stdio.h>
#include<stdlib.h>
int main(){
	float x[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	float y[10] = {5.5 , 7.0,  9.6,  11.5, 12.6, 14.4, 17.6, 19.5, 20.5};
	float sx=0, sy=0, sxy=0, ssqx=0, a1, a2, n1, d1, n2, d2;
	printf("x = 1, 2, 3, 4, 5, 6, 7, 8, 9 \nf(x) = 5.5 , 7.0,  9.6,  11.5, 12.6, 14.4, 17.6, 19.5, 20.5\n");
	printf("The equation that fits best to the above data is:\n");
	for(int i=0; i<9; i++){
		sx = sx + x[i];
		sy = sy + y[i];
		ssqx = ssqx +( x[i]*x[i]);
	    sxy = sxy + (y[i]*x[i]);
	}
	n1 = ((sy*ssqx)-(sx*sxy));
	d1 = ((9*ssqx)-(sx*sx));
	a1 = n1/d1;
	n2 = ((9*sxy)-(sx*sy));
	d2 = ((9*ssqx) - (sx*sx));
	a2 = n2/d2;
	printf("A straight line given as: y = %.3fx + %.3f\n", a2, a1);
	return 0;
}