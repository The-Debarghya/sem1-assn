#include<stdio.h>
#include<stdlib.h>
#include<math.h>
int fact(int n){ 
    int f = 1; 
    for (int i = 2; i <= n; i++) 
        f *= i; 
    return f; 
} 
float u_fwd(float u, int n){ 
    float temp = u; 
    for (int i = 1; i < n; i++) 
        temp = temp * (u - i); 
    return temp; 
} 
float u_bwd(float u, int n){ 
    float temp = u; 
    for (int i = 1; i < n; i++) 
        temp = temp * (u + i); 
    return temp; 
} 
void newton_fwd(float *x, float *y, int n, float xi){
	float z[n+1][n+1], sum, u;
	for(int i=0; i<=n; i++){
		z[i][0] = y[i];
	}
	for (int i = 1; i < n; i++) { 
        for (int j = 0; j < n - i; j++){ 
            z[j][i] = z[j + 1][i - 1] - z[j][i - 1];
		}
    } 
	sum = z[0][0];  
	u = (xi - x[0]) / (x[1] - x[0]);
	for (int i = 1; i <= n; i++) { 
        sum = sum + (u_fwd(u, i) * z[0][i])/fact(i);
    } 
	 printf("%f\n", sum);
}
void newton_bwd(float *x, float *y, int n, float xi){
	float z[n][n], sum, u;
	for(int i=0; i<=n; i++){
		z[i][0] = y[i];
	}
	for (int i = 1; i < n; i++) { 
        for (int j = n - 1; j >= i; j--){ 
            z[j][i] = z[j][i - 1] - z[j - 1][i - 1]; 
		}
    } 
	sum = z[n - 1][0];
	u = (xi - x[n - 1]) /(x[1] - x[0]); 
	for (int i = 1; i < n; i++) { 
        sum = sum + (u_bwd(u, i) * z[n - 1][i])/fact(i); 
    }
	printf("%f\n", sum);
}
void lagrange(float *x, float *y, int n, float xi){
	float sum = 0, prod=1;
	for (int i=0; i<=n; i++){
		prod = 1;
		for(int j=0; j<=n; j++){
			if(j != i){
				prod = prod*(xi - x[j])/(x[i] - x[j]);
			}
		}
		sum = sum + y[i]*prod;
	}
	printf("%f\n", sum);
}
int main(){
	float x[20], y[20], xi;
	int n, a, b;
	printf("No. of data points you want to enter:");
	scanf("%d", &b);
	printf("Enter the %d data points and the respective values of the function:\n", b);
	n = b-1;
	for(int i=0; i<=n; i++){
		printf("x%d=", i+1);
		scanf(" %f", &x[i]);
		printf("f(x%d)=", i+1);
		scanf(" %f", &y[i]);
	}
	printf("Enter the point at which you want to interpolate:");
	scanf(" %f", &xi);
	st: printf("By which method you want to interpolate \n(1. Newton's forward method, 2. Newton's backward method, 3. Lagrange's method)");
	scanf(" %d", &a);
	if(a == 1){
		newton_fwd(x, y, b, xi);
	}
	else if(a == 2){
		newton_bwd(x, y, b, xi);
	}
	else if(a == 3){
		lagrange(x, y, b, xi);
	}
	else{
		printf("Wrong input\n");
		goto st;
	}
	char ch;
	printf("Do you want to continue?(y/n)");
	scanf( " %c", &ch);
	if(ch == 'y'){
		main();
	}
	else{
		exit(0);
	}
	return 0;
}
	
