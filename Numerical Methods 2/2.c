#include<stdio.h>
#include<stdlib.h> 
#include<math.h>
void gauss_jordan( float a[4][4], int var ){
    float l;
    for ( int k = 0;k < var;k++ ){
        for ( int i = 0;i <= var;i++ ){
            l = a[ i ][ k ];
            for ( int j = 0;j <= var;j++ ){
                if ( i != k ){
                a[i][j] = (a[k][k]*a[i][j])-(l*a[k][j]);
				}
            }
        }
    }
    printf( "Solutions by Gauss Jordan method:\n" );
    for ( int i = 0;i < var; i++ ){
        printf( "The value of x%d is = %.2f\n", i + 1, ( float ) a[ i ][ var ] / ( float ) a[ i ][ i ] );
    }
}
void gauss_elim(float a[4][4], int var){
	int i, j, k;
	float p, x[20], s;
	for(k=0; k<=var-1; k++){
	for(i=k+1; i<var; i++){
	    p = a[i][k]/a[k][k];
	    for(j=k; j<=var; j++){
		a[i][j] = a[i][j] - (p * a[k][j]);
	    }
	}
    }
    x[var-1] = a[var-1][var] / a[var-1][var-1];
    for(i=var-2; i>=0; i--){
        s=0;
        for(j=i+1; j<var; j++){
            s += (a[i][j]*x[j]);
            x[i] = (a[i][var]-s)/a[i][i];
        }
    }
    printf("Solutions by naive Gauss elimination method:\n");
    for(i=0; i<var ; i++){
        printf("The value of x%d is = %.2f\n",i+1,x[i]);
    }
}
void jacobi(float a[4][4], int var, float e, int n){
	float big, x[20], x1[20], sum, temp, relerr;
	for(int i=0; i<var; i++){
		x[i] = 0;
	}
	for(int j=1; j<=n; j++){
		big = 0;
		for(int i=0; i<var; i++){
			sum = 0;
			for(int k=0; k<var; k++){
				if(k != i){
					sum = sum + a[i][k]*x[k];
				}
			}
			temp = (a[i][var] - sum)/a[i][i];
			relerr = fabs((x[i] - temp)/temp);
			if(relerr > big){
				big = relerr;
			}
            
			x1[i] = temp;
		}
		for(int i=0; i<var; i++){
			x[i] = x1[i];
		}
		if (big <= e){
			printf("Solutions by Jacobi's Method:\n");
			for(int l=0; l<var; l++){
				printf("x%d = %f \n", l+1, x[l]);
			}
			return ;
		}
	}
	printf("Solutions doesn't converge in %d no. of iterations", n);
}
int main(){
    int x, n;
	float e;
	float a[4][4] = {1.0, 1.0, 1.0, 6.0, 1.0, -1.0, 1.0, 0.0, 1.0, 1.0, -1.0, 2.0 };
	float b[4][4] = {{1.0, 1, 1, 3}, {2, 3, 1, 6}, {1, -1, -1, -3}};
	float c[4][4] = {{2.0, 4, 2, 15}, {2, 1, 2, -5}, {4, 1, -2, 0}};
	printf("Equation set1: x+y+z=6\n x-y+z=0 \n x+y-z=2\n Equation set2: x+y+z=3\n 2x+3y+z=6\n x-y-z=-3\n Equation set3: 2x+4y+2z=15\n 2x+y+2z=-5 \n 4x+y-2z=0\n");
   
	st : printf("Which system of equations you want to solve? \n(1., 2. ,3. )");
	scanf(" %d", &x);
	printf("Enter the permissible error:");
	scanf(" %f", &e);
	if(x == 1){
		gauss_elim(a, 3);
		gauss_jordan( a, 3);
		jacobi(a, 3, e, 1000);
	}
	else if(x == 2){
		gauss_elim(b, 3);
		gauss_jordan( b, 3 );
		jacobi(b, 3, e, 100);
	}
	else if(x == 3){
		gauss_elim(c, 3);
		gauss_jordan( c, 3 );
		jacobi(c, 3, e, 100);
	}
	else{
		printf("Wrong input\n");
		goto st;
	}
    char y;
    printf("Do you want to continue?(y/n)");
    scanf(" %c", &y);
    if(y == 'y'){
        main();
    }
    else{
        exit(0);
    }
    return 0;
}
 
 
 
