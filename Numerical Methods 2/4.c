#include<stdio.h>
#include<stdlib.h>
#include<math.h>
int fact(int n){ 
    int f = 1; 
    for (int i = 2; i <= n; i++) 
        f *= i; 
    return f; 
} 
float u_fwd(float u, int n){ 
    float temp = u; 
    for (int i = 1; i < n; i++) 
        temp = temp * (u - i); 
    return temp; 
} 
float u_bwd(float u, int n){ 
    float temp = u; 
    for (int i = 1; i < n; i++) 
        temp = temp * (u + i); 
    return temp; 
} 
void newton_fwd(float *x, float *y, int n, float xi){
	float z[n+1][n+1], sum=0, u;
	for(int i=0; i<=n; i++){
		z[i][0] = y[i];
	}
	for (int i = 1; i < n; i++) { 
        for (int j = 0; j < n - i; j++){ 
            z[j][i] = z[j + 1][i - 1] - z[j][i - 1];
		}
    } 
	sum = z[0][0];  
	u = (xi - x[0]) / (x[1] - x[0]);
	for (int i = 1; i <= n; i++) { 
        sum = sum + (u_fwd(u, i) * z[0][i])/fact(i);
    } 
	 printf(" By Newton's forward interpolation f(%f)=%f\n",xi, sum);
}
void newton_bwd(float *x, float *y, int n, float xi){
	float z[n][n], sum, u;
	for(int i=0; i<=n; i++){
		z[i][0] = y[i];
	}
	for (int i = 1; i < n; i++) { 
        for (int j = n - 1; j >= i; j--){ 
            z[j][i] = z[j][i - 1] - z[j - 1][i - 1]; 
		}
    } 
	sum = z[n - 1][0];
	u = (xi - x[n - 1]) /(x[1] - x[0]); 
	for (int i = 1; i < n; i++) { 
        sum = sum + (u_bwd(u, i) * z[n - 1][i])/fact(i); 
    }
	printf(" By Newton's backward interpolation f(%f)=%f\n",xi, sum);
}
void lagrange(float *x, float *y, int n, float xi){
	float sum = 0, prod=1;
	for (int i=0; i<=n; i++){
		prod = 1;
		for(int j=0; j<=n; j++){
			if(j != i){
				prod = prod*(xi - x[j])/(x[i] - x[j]);
			}
		}
		sum = sum + y[i]*prod;
	}
	printf(" By Lagrange interpolation f(%f)=%f\n",xi, sum);
}
int main(){
	int  a;
	float x[5] = {1, 2, 3, 4};
	float y[5] = {1, 8, 27, 64};
	printf("x = %.2f  %.2f   %.2f   %.2f\n", x[0], x[1], x[2], x[3]);
	printf("f(x) = %.2f  %.2f %.2f %.2f\n", y[0], y[1], y[2], y[3]);
	st: printf("By which method you want to interpolate \n(1. Newton's forward method, 2. Newton's backward method, 3. Lagrange's method)");
	scanf(" %d", &a);
	if(a == 1){
		newton_fwd(x, y, 4, 2.5);
	}
	else if(a == 2){
		newton_bwd(x, y, 4, 2.5);
	}
	else if(a == 3){
		lagrange(x, y, 4, 2.5);
	}
	else{
		printf("Wrong input\n");
		goto st;
	}
	char ch;
	printf("Do you want to continue?(y/n)");
	scanf( " %c", &ch);
	if(ch == 'y'){
		main();
	}
	else{
		exit(0);
	}
	return 0;
}
	
