#include<stdio.h>
#include<math.h>
float func1(float x, float y){
    return (2*x*y);
}
float func2(float x, float y){
    return(x*x + y*y);
}
void euler(float x1, float y1, float xf, int x){
    float x2, y2, h=0.01, e;
    while(x1 <= xf){
        x2 = x1 + h;
        if (x == 1){
            y2 = y1 + h*func1(x1, y1);
        }
        else{
            y2 = y1 + h*func2(x1, y1);
        }
        x1 = x2;
        y1 = y2;
    }
    printf("The approx. value of y at %f is %f\n", xf, y1);
}
void rungekutta(float x1, float y1, float xf, int x){
    float h, x2, y2, s1, s2;
    h = (xf-x1)/100;
    for(int i=1; i<=100; i++){
        x2 = x1+h;
        if(x == 1){
            s1 = h*func1(x1, y1);
            s2 = h*func1(x2, y1+s1);
        }
        else{
            s1 = h*func1(x1, y1);
            s2 = h*func1(x2, y1+s1);
        }
        y2 = y1 + 0.5*(s1+s2);
        y1 = y2;
        x1 = x2;
    }
    printf("The approx. value of y at %f is %f\n", xf, y1);
}
int main(){
    printf("The differential equation(dy/dx = 2xy) solved by 2 different methods given below:\n");
    printf("By Euler's method\n");
    euler(0, 0.5, 1, 1);
    printf("By Runge Kutta second order method\n");
    rungekutta(0, 0.5, 1, 1);
    printf("The differential equation(dy/dx = x^2+y^2) solved by 2 different methods given below:\n");
    printf("By Euler's method\n");
    euler(0, 1, 1, 2);
    printf("By Runge Kutta second order method\n");
    rungekutta(0, 1, 1, 2);
    printf("We see that in first equation in both methods the estimation is close enough,\n but in second equation the Euler's method gets highly differed\n");
    return 0;        
}