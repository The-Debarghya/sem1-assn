#include<stdio.h>
#include<math.h>
void forward(float xi, float delx){
    float err, dydx, dydx1;
    dydx1 = (sinf(xi+delx)-sinf(xi))/delx;
    dydx = cosf(xi);
    err = fabs((dydx - dydx1)/dydx)*100.0;
    printf("\nDerivative calculated by forward difference method(taking h=%.2f):\n", delx);
    printf("actual derivative:%f\ncalculated derivative:%f\nrelative error:%f\n", dydx, dydx1, err);
}
void backward(float xi, float delx){
    float err, dydx, dydx1;
    dydx1 = (sinf(xi-delx)-sinf(xi))/-(delx);
    dydx = cosf(xi);
    err = fabs((dydx - dydx1)/dydx)*100.0;
    printf("\nDerivative calculated by backward difference method(taking h=%.2f):\n", delx);
    printf("actual derivative:%f\ncalculated derivative:%f\nrelative error:%f\n", dydx, dydx1, err);
}
void central(float xi, float delx){
    float err, dydx, dydx1;
    dydx1 = (sinf(xi+delx)-sinf(xi-delx))/(2.0*delx);
    dydx = cosf(xi);
    err = fabs((dydx - dydx1)/dydx)*100.0;
    printf("\nDerivative calculated by central difference method(taking h=%.2f):\n", delx);
    printf("actual derivative:%f\ncalculated derivative:%f\nrelative error:%f\n", dydx, dydx1, err);
}
int main(){
    float xi;
    printf("Enter the point where dy/dy(sin(x)) is required:");
    scanf("%f", &xi);
    forward(xi, 0.1);
    backward(xi, 0.1);
    central(xi, 0.1);
    forward(xi, 0.01);
    backward(xi, 0.01);
    central(xi, 0.01);
    printf("From the above results it is clear that central difference scheme yields\nbetter result(less error) than forward and backward\n");
    return 0;
}