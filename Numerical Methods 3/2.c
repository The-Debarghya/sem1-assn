#include<stdio.h>
#include<stdlib.h>
#include<string.h>
FILE *fp;
int main(int argc, char **argv){
    float a, b;
    if(argc <= 3){
        printf("Too few arguments given\n");
        exit(0);
    }
    a = atof(argv[2]);
    b = atof(argv[3]);
    fp = fopen("runtime.c", "w+");
    fprintf(fp, "#include<stdio.h> \n#include<math.h> \n float func(float x){ \n return %s;}\n", argv[1]);
    fprintf(fp, "void simpson(float a, float b, float h){\n float sum=0, res_=0;\nfor(int i=1; i<100; i++){\n if(i%2 == 0){\n sum = sum + 2*func(a + h*i);\n  } else{\n sum = sum + 4*func(a + h*i);\n } } sum = sum + func(a)+func(b);\n res_=h*sum/3;\n printf(\"%%f\\n\", res_);}\n" );
    fprintf(fp, "void trapezoidal(float a, float b, float h){\n\n float sum=0, res=0;\n\n for(int i=1; i<100; i++){\n \n \t sum = sum + 2*func(a + h*i); \n  } sum = sum + func(a)+func(b);\n  res = h*sum/2;\n  \n printf(\"%%f\\n\", res);\n}\n");
    fprintf(fp, "int main(){\n\n float h;\n h = (%f-(%f))/100;\n printf(\"Integral of the function by trapezoidal method is:\\n\");\n trapezoidal(%f, %f, h);\n printf(\"Integral of the function by Simpson's method is:\\n\");\n simpson(%f, %f, h);\n return 0;\n}", b, a, a, b, a, b);
    fclose(fp);
    system("gcc runtime.c -o runtime -lm \n");
    system("./runtime");
    remove("runtime.c");
    remove("runtime");
    return 0;
}