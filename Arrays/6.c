//Write a C program which accepts numbers obtained by five students in five subjects. Print the total marks obtained by all the students. Also determine the highest total marks.
#include<stdio.h>
int main(){
	int marks[5][5], a[5], max;
	a[5] = (0);
	printf("Enter marks of 5 students in 5 subjects :");
	for(int i=0; i<5; i++){
		for(int j=0; j<5; j++){
			scanf("%d", &marks[i][j]);
		}
	}
	for(int k=0; k<5; k++){
		a[k] = 0;
		for(int l=0; l<5; l++){
			a[k] = a[k] + marks[k][l];
		}
	}
	max = a[0];
	for(int j=1; j<5; j++){
		if(a[j] > max){
			max = a[j];
		}
		else{
			continue;
		}
	}
	printf("Total marks of each student respectively: ");
	for(int m=0; m<5; m++){
		printf("%d ", a[m]);
	}
	printf("\n The maximum total marks is: %d", max);
	return 0;
}
