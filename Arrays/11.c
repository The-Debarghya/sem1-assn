//Write a program which takes some numbers and computes the standard deviation of them.
#include<stdio.h>
#include<math.h>
int main(){
	float a[100], avg, total=0, sos=0, s, std, d=0;
	int n;
	printf("Enter no. of entries ");
	scanf("%d", &n);
	printf("Enter %d entries:", n);
	for(int i=0; i<n; i++){
		scanf("%f", &a[i]);
	}
	for(int j=0; j<n; j++){
		total = total + a[j];
	}
	avg = total/n;
	for(int k=0; k<n; k++){
		d = a[k] - avg;
		sos = sos + pow(d, 2);
		d = 0;
	}

	std = sqrt(sos/n);
	s = sqrt(sos/(n-1));
	printf("Standard deviation of the data is: %f \n", std);
	printf("Unbiased estimator of variance(%f) of the data is: %f \n", pow(std, 2), s);
	return 0;
}
