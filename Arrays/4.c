//Write a program to find the range of a set of integers entered by the user. Range is the difference between the smallest and biggest number in the list.
#include<stdio.h>
#include<stdlib.h>
int main(){
	int *arr, max, min, n;
	printf("Enter no. of integers you want to input ");
	scanf("%d", &n);
	arr = (int *)(malloc(n*sizeof(int)));
	printf("Enter the integers ");
	for(int i=0; i<n; i++){
		scanf("%d", &arr[i]);
	}
	max = arr[0];
	for(int j=1; j<n; j++){
		if(arr[j] > max){
			max = arr[j];
		}
		else{
			continue;
		}
	}
	min = arr[0];
	for(int j=1; j<n; j++){
		if(arr[j] < min){
			min = arr[j];
		}
		else{
			continue;
		}
	}
	free(arr);
	printf("The range of the set is %d", max-min);
	return 0;
}
