//Write a C program which accepts a matrix and prints its transpose.
#include<stdio.h>
int main(){
	int m[100][100], tr[100][100], row, column;
	printf("Enter no. of rows ");
	scanf("%d", &row);
	printf("Enter no. of columns ");
	scanf("%d", &column);
	printf("Enter the elements of the matrix(row-major wise) ");
	for(int i=0; i < row; i++){
		for(int j=0; j < column; j++){
			scanf("%d", &m[i][j]);
		}
	}
	for(int k=0; k < column; k++){
		for(int l=0; l < row; l++){
			tr[k][l] = m[l][k];
		}
	}
	printf("The transpose of the matrix is: \n");
	for(int i=0; i < column; i++){
		for(int j=0; j < row; j++){
			printf("%d ", tr[i][j]);
		}
		printf("\n");
	}
	return 0;
}
