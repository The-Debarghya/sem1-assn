//Write a C program which accepts roll numbers of ten students and marks obtained by them in five subjects and prints the names of the students who have obtained highest and second highest marks subject wise.
#include<stdio.h>
int main(){
int  a[10][5], max[5], max2[5], b=0, c=0;
printf("Enter marks of 5 subjects roll-wise \n");
for(int i=0,j=1; i<10, j<=10; i++, j++){
    printf("Enter marks of roll %d ", j);
    for(int l=0; l<5; l++){
        scanf("%d", &a[i][l]);
        
        }
    
    }
for(int k=0; k<5; k++){
    max[k] = a[0][k];
    for(int m=0; m<10; m++){
       if(a[m][k] > max[k]){
          max[k] = a[m][k];
          
          }
       else{
         continue;
         }
       }
    max2[k] = a[0][k];
    for(int m=0; m<10; m++){
       if(a[m][k] > max2[k] && a[m][k] < max[k]){
          max2[k] = a[m][k];
          
          }
       else{
         continue;
         }
       }
    
    
    }
    for(int n=0; n<5; n++){
        
        for(b=0; b<10; b++){
            
            if(max[n] == a[b][n])
               break;
        }
        printf("Roll %d got highest marks in subj %d \n", b+1, n+1);       
        
        
    }
    for(int n=0; n<5; n++){
        
        for(c=0; c<10; c++){
            
            if(max2[n] == a[c][n])
               break;
        }
        printf("Roll %d got 2nd highest marks in subj %d \n", c+1, n+1);       
        
        
    }

return 0;
}
