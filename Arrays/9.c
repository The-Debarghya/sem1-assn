//Write a C program to replace a square matrix by its transpose without using a second matrix.
#include<stdio.h>
int main(){
	int m[100][100], n, t=0;
	printf("Enter no. of rows of the square matrix ");
	scanf("%d", &n);
	printf("Enter the elements of %d x %d matrix ", n, n);
	for(int i=0; i<n; i++){
		for(int j=0; j<n; j++){
			scanf("%d", &m[i][j]);
		}
	}
	printf("The matrix: \n");
	for(int i=0; i<n; i++){
		for(int j=0; j<n; j++){
			printf("%d ", m[i][j]);
		}
		printf("\n");
	}
	for(int i=0; i<n; i++){
		for(int j=0; j<n; j++){
			t=m[i][j];
			m[i][j]=m[j][i];
			m[j][i]=t;
		}
	}
	printf("Its transpose: \n");
	for(int i=0; i<n; i++){
		for(int j=0; j<n; j++){
			printf("%d ", m[j][i]);
		}
		printf("\n");
	}
	return 0;
}
