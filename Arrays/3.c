//Write a menu-driven program for accepting values in two square matrices of 3x3 dimension and generate their sum, difference and product.
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
void sum(int m1[3][3], int m2[3][3], int sum[3][3]){
	for(int i=0; i<=2; i++){
		for(int j=0; j<=2; j++){
			sum[i][j] = m1[i][j] + m2[i][j];
		}
	}
	for(int k=0; k<=2; k++){
		for(int l=0; l<=2; l++){
			printf("%d ", sum[k][l]);
		}
		printf("\n");
	}
}
void diff(int m1[3][3], int m2[3][3], int diff[3][3]){
	for(int i=0; i<=2; i++){
		for(int j=0; j<=2; j++){
			diff[i][j] = m1[i][j] - m2[i][j];
		}
	}
	for(int k=0; k<=2; k++){
		for(int l=0; l<=2; l++){
			printf("%d ", diff[k][l]);
		}
		printf("\n");
	}
}
void prod(int m1[3][3], int m2[3][3], int prod[3][3]){
	for(int i=0; i<=2; i++){
		for(int j=0; j<=2; j++){
			prod[i][j] = (0);
			for(int m=0; m <= 2; m++){
				prod[i][j] = prod[i][j] + m1[i][m]*m2[m][j];
			}
		}
	}
	for(int k=0; k<=2; k++){
		for(int l=0; l<=2; l++){
			printf("%d ", prod[k][l]);
		}
		printf("\n");
	}
}
int main(){
	int m1[3][3], m2[3][3], s[3][3], d[3][3], p[3][3];
	int n; fflush(stdin);
	char c;
	printf("Enter 1st matrix(row-major wise) ");
	for(int i=0; i<=2; i++){
		for(int j=0; j<=2; j++){
			scanf("%d", &m1[i][j]);
		}
	}
	printf("Enter 2nd matrix(row-major wise) ");
	for(int i=0; i<=2; i++){
		for(int j=0; j<=2; j++){
			scanf("%d", &m2[i][j]);
		}
	}
	a: printf("\n What operation you want to do(1. sum, 2. difference, 3. product) \n");
	
	scanf("%d", &n);
        if(n == 1){
    	printf("The sum of the matrices is: \n"); sum(m1, m2, s);
	}
	else if(n == 2){
    	printf("The difference of the matrices is: \n"); diff(m1, m2, d);
	}
	else if(n == 3){
    	printf("The product of the matrices is: \n"); prod(m1, m2, p);
	}
	else{
		printf("Wrong input");
		goto a;
	}fflush(stdin);
	printf("Do you want to continue?(y/n)");
	scanf(" %c", &c);
	if(c == 'y')
	   main();
	else
	   exit(0);
	return 0;
}
