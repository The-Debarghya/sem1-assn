//Write a program in C to reverse the contents of the elements of an integer array.
#include<stdio.h>
#include<stdlib.h>
int main(){
	int *arr, n;
	printf("No. of elements you want to input ");
	scanf("%d", &n);
	arr = (int *)(malloc(n*sizeof(int)));
	printf("Enter the elements of the array ");
	for(int j=0; j<n; j++){
	        scanf("%d", &arr[j]);
	}
	printf("The elements in reverse order ");
	for(int i=n-1; i >= 0; i--){
		printf("%d ", arr[i]);
	}
	printf("\n");
	free(arr);
	return 0;
}
