#include<stdio.h>
#include<stdlib.h>
#include<math.h>
float func(float x){
	return (x*x*x - 4*x - 3);
}
float funcd(float x){
	return (3*x*x - 4);
}
void bisection(float x0, float x1, float e, int n){
	float y0, y1, y2, x2;
	y0 = func(x0);
	y1 = func(x1);
	for(int i=0; i<=n; i++){
		x2 = (x0 + x1)/2;
		y2 = func(x2);
		if(fabs(y2) <= e){
			printf("Convergent solution: f(%f)=%f \n", x2, y2);
			return;
		}
		if((y0*y2) > 0){
			x0 = x2;
			y0 = y2;
		}
		else{
			x1 = x2;
			y1 = y2;
		}
	}
	printf("Solution doesn't converge in %d iterations; f(%f) = %f\n", n, x2, y2);
}
void falseposition(float x0, float x1, float e, int n){
	float y0, y1, y2, x2;
	y0 = func(x0);
	y1 = func(x1);
	for(int i=1; i<=n; i++){
		x2 = ((x0*y1)-(x1*y0))/(y1-y0);
		y2 = func(x2);
		if(fabs(y2) <= e){
			printf("Convergent solution: f(%f)=%f \n", x2, y2);
			return;
		}
		if((y0*y2) < 0){
			x1 = x2;
			y1 = y2;
		}
		else{
			x0 = x2;
			y0 = y2;
		}
	}
	printf("Solution doesn't converge in %d iterations; f(%f) = %f\n", n, x2, y2);
}
void newtonraphson(float x0, float e, float d, int n){
	float x1, yd0, y0;
	for(int i=1; i<=n; i++){
		y0 = func(x0);
		yd0 = funcd(x0);
		if(fabs(yd0) <= d){
			printf("Slope too small; f'(%f)=%f\n", x0, yd0);
			return;
		}
		x1 = x0 - (y0/yd0);
		if(fabs((x1-x0)/x1) <= e){
			printf("Convergent Solution; f(%f)=%f \n", x1, func(x1));
			return;
		}
		x0 = x1;
	}
	printf("Solution doesn't converge in %d iterations; f(%f)=%f, f'(%f)=%f, at (%f, 0)\n", n, x0, y0, x0, yd0, x1);
}
		
int main(){
	int n, x;
	float x0, x1, e, d;
	printf("Which Method you want to calculate? (1. Bisection 2. False Position 3. Newton Raphson)");
	scanf("%d", &x);
	if(x == 1){
		printf("Enter the 2 initial guesses ");
	    scanf("%f %f", &x0, &x1);
	    printf("Enter the no. of iterations: ");
	    scanf("%d", &n);
	    printf("Permissible error:");
	    scanf("%f", &e);
		bisection(x0, x1, e, n);
	}
	else if(x == 2){
		printf("Enter the 2 initial guesses ");
	    scanf("%f %f", &x0, &x1);
	    printf("Enter the no. of iterations: ");
	    scanf("%d", &n);
	    printf("Permissible error:");
	    scanf("%f", &e);
		falseposition(x0, x1, e, n);
	}
	else if(x == 3){
		printf("Enter the initial guess ");
		scanf("%f", &x0);
		printf("The permissible limit of slope ");
		scanf("%f", &d);
		printf("The permissible error ");
		scanf("%f", &e);
		printf("No. of iterations:");
		scanf("%d", &n);
		newtonraphson(x0, e, d, n);
	}
	else{
		printf("Wrong input \n");
		main();
	}
	char c;
	printf("Do you want to continue?(y/n)");
	scanf(" %c", &c);
	if(c == 'y'){
		main();
	}
	else{
		exit(0);
	}
	return 0;
}