#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define PI 3.14159265359
float func(float x, int c){
	return (x*tan(x)- c);
}
float funcd(float x){
	return (tan(x) + x*(1/(cos(x)*cos(x))));
}
void bisection(float x0, float x1, float e, int c){
	float y0, y1, y2, x2;
	y0 = func(x0, c);
	y1 = func(x1, c);
	for(int i=0; i<=100; i++){
		x2 = (x0 + x1)/2;
		y2 = func(x2, c);
		if(fabs(y2) <= e){
			printf("Approx. solution(x=%f): f(%f)=%f \n", x2, x2, y2);
			return;
		}
		if((y0*y2) > 0){
			x0 = x2;
			y0 = y2;
		}
		else{
			x1 = x2;
			y1 = y2;
		}
	}
	printf("Solution doesn't converge; f(%f) = %f\n", x2, y2);
}
void newtonraphson(float x0, float e, int c){
	float x1, yd0, y0;
	for(int i=1; i<=100; i++){
		y0 = func(x0, c);
		yd0 = funcd(x0);
		if(fabs(yd0) <= 0.00005){
			printf("Slope too small; f'(%f)=%f\n", x0, yd0);
			return;
		}
		x1 = x0 - (y0/yd0);
		if(fabs((x1-x0)/x1) <= e){
			printf("Approx. Solution; f(%f)=%f \n", x1, func(x1, c));
			return;
		}
		x0 = x1;
	}
	printf("Solution doesn't converge; f(%f)=%f, f'(%f)=%f, at (%f, 0)\n", x0, y0, x0, yd0, x1);
}
int main(){
	int c;
	float e;
	printf("xtanx = c (input c)");
	scanf("%d", &c);
	printf("Permissible error ");
	scanf("%f", &e);
	printf("Solutions of f(x)= xtanx -%d by bisection method:\n", c);
	bisection(0, 2, e, c);
	bisection(PI, 2+PI, e, c);
	bisection(2*PI, 2+2*PI, e, c); printf("\n");
	printf("Solutions of f(x)= xtanx -%d by newton raphson method:\n", c);
	newtonraphson(1, e, c);
	newtonraphson(1+PI, e, c);
	newtonraphson(1-PI, e, c);
	return 0;
}