#include<stdio.h>
#include<stdlib.h>
#include<math.h>
float func(float x){
	return(x*x*x - 2.5*x*x -2.46*x+3.96);
}
float funcd(float x){
	return(3*x*x-5*x-2.46);
}
void newtonraphson(float x0, float e){
	float x1, yd0, y0;
	for(int i=1; i<=100; i++){
		y0 = func(x0);
		yd0 = funcd(x0);
		if(fabs(yd0) <= 0.00005){
			printf("Slope too small; f'(%f)=%f\n", x0, yd0);
			return;
		}
		x1 = x0 - (y0/yd0);
		if(fabs((x1-x0)/x1) <= e){
			printf("Approx. Solution; f(%f)=%f \n", x1, func(x1));
			return;
		}
		x0 = x1;
	}
	printf("Solution doesn't converge; f(%f)=%f, f'(%f)=%f, at (%f, 0)\n", x0, y0, x0, yd0, x1);
}
int main(){
	float x0, x1, x2, r1, r2, e, D;
	printf("Enter the permissible error ");
	scanf("%f", &e);
	D = sqrtf(25 - 4*3*(-2.46));
	r1 = (5-D)/6;
	r2 = (5+D)/6;
	
	printf("The 3 roots lies in the following intervals respectively: [-4, %f), (%f, %f), (%f, 4] \n", r1, r1, r2, r2);
	printf("The 3 solutions of the equation x^3 -2.5x^2 -2.46x+3.96=0, by Newton Raphson Method is as follows:\n");
	x0 = (r2+4)/2;
	newtonraphson(x0, e);
	x1 = (r1+r2)/2;
	newtonraphson(x1, e);
	x2 = (-4+r1)/2;
	newtonraphson(x2, e);
	return 0;
}
	