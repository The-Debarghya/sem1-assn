#include<stdio.h>
int charcount(char *str){
	int s=0, i=0;
	while(str[i] != '\0'){
		s = s+1;
		i = i+1;
	}
	return (s);
}
void reverse(char *str, char *str_rev){
	int n;
	n = charcount(str);
	for(int i=0, j=n-1; i<n, j>=0; i++, j--){
		str_rev[j] = str[i];
	}
	str_rev[n] = '\0';
}
int wordcount(char *str){
	int ct=0, n;
	n = charcount(str);
	for(int i=0; i<n; i++){
		if(str[i] == ' ' && str[i+1] != ' '){
			ct = ct+1;
		}
		else
			continue;
	}
	return (ct+1);
}
void deletespace(char *str, char *s){
	int n, j=0;
	n = charcount(str);
	for(int i=0; i<n; i++){
		if(str[i] == ' '){
			continue;
		}
		else{
			s[j] = str[i];
			j = j+1;
		}
	}
	s[j] = '\0';
}
int main(){
	char s[1000], r[1000], ns[1000];
	int n, c=0;
	printf("Enter the string: ");
	scanf("%[^\n]s", s);
	deletespace(s, ns);
	reverse(ns, r);
	n = charcount(ns);
	for(int i=0; i<n; i++){
		if(ns[i] != r[i]){
			printf("Not a pallindrome \n");
			break;
		}
		else
			c = c+1;
	}
	if(c == n){
		printf("Its a pallindrome \n");
	}
	printf("No. of characters: %d \n", charcount(s));
	printf("No. of words: %d \n", wordcount(s));
    return 0;
}
