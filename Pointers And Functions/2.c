#include<stdio.h>
#include<string.h>
int strcount(char *str){
	int ct=0, i=0;
	while(str[i] != '\0'){
		ct = ct+1;
		i = i+1;
	}
	return ct;
}
void reverse(char *str){
	int n;
	n = strcount(str);
	for(int i=n; i>=0; i--){
		printf("%c", str[i]);
	}
	printf("\n");
}
int main(){
	char s[1000];
	printf("Enter the string ");
	scanf("%[^\n]s", s);
	printf("Length of the string is %d \n", strcount(s));
	printf("Reverse of the string: "); reverse(s);
	return 0;
}
